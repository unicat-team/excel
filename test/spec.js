import * as xlsx from 'node-xlsx';
import StandardExcelBuilder from '../src/excel-builder';
import ObjectBuilder from '../src/builder/object-builder';
import ListBuilder from '../src/builder/list-builder';
import BuildHandler from '../src/builder/build-handler';
import Required from '../src/validator/required-validator';
import Unique from '../src/validator/unique-validator';
import { assert } from 'chai';
import { readFileSync } from 'fs';
import {full} from './fixtures/excel-data';

import i18n from '../src/i18n';

let buffer;

before(() => {
  buffer = xlsx.build([{name: "mySheetName", data: full}]);
});

describe('spec', () => {
  it('nake excel', () => {
    const excel = new StandardExcelBuilder(buffer).excel();
    const t = i18n('ru');
    
    const validator = new BuildHandler(new Required(t('Category')), new Required(t('Product Name')), new Required(t('Vender Code')), new Unique(t('Vender Code')));
    
    const categoryBuilder = new ObjectBuilder()
      .add(t('Category'), name => ({name}))
    
    const productBuilder = new ObjectBuilder()
      .add(t('Product Name'), name => ({name}))
      .add(t('Vender Code'), vendorCode => ({vendorCode}))
      .add(t('Brand'), brand => ({brand}))
      .add(t('Url'), url => ({url}))
      .add(t('Description'), description => ({description}))
      .add(t('EAN'), ean => ({ean}))
      .add(t('MPN'), mpn => ({mpn}));
    
    const picturesBuilder = new ListBuilder()
      .add(t('Product picture'), src => ({src, type: 'picture'}))
  
    const offerBuilder = new ObjectBuilder()
      .add(t('Price'), price => ({price}))
      .add(t('Currency'), currency => ({currency}))
      .add(t('Sales Price'), salesPrice => ({salesPrice}));
      
    
    const mediaBuilder = new ListBuilder()
      .add(t('Youtube'), src => ({src, type: 'youtube'}))
      .add(t('Soundcloud'), src => ({src, type: 'soundcloud'}))
      .add(t('Picture'), src => ({src, type: 'picture'}))
      .add(t('PDF'), src => ({src, type: 'pdf'}));
    
    const wdDescriptionBuilder = new ObjectBuilder()
      .add(t('Physical parameters description'), description => ({description}))
    
    const dimensionsBuilder = new ObjectBuilder()
      .add(t('Physical parameters width'), width => ({width}))
      .add(t('Physical parameters height'), height => ({height}))
      .add(t('Physical parameters depth'), height => ({height}))
      .add(t('Physical parameters unit'), height => ({height}));
    
    const weightBuilder = new ObjectBuilder()
      .add(t('Physical parameters weight'), value => ({value}))
      .add(t('Physical parameters weight unit'), unit => ({unit}));
      
    const builders = [
      categoryBuilder, 
      productBuilder, 
      picturesBuilder, 
      offerBuilder, 
      mediaBuilder, 
      wdDescriptionBuilder, 
      dimensionsBuilder, 
      weightBuilder];
    const usedHeaders = Array.prototype.concat.apply([], builders.map(builder => 
      builder.mappings.map(mapping => mapping.name)))
    const unusedHeaders = excel.headers.map(header => header.name).filter(header => usedHeaders.every(used => used !== header));
    console.log('unused', unusedHeaders);
    
    
      
    // const allMappings = Array.prototype.concat.call([], 
    //   productPicturesBuilder.mappings, )
      
    for (let row of excel.rows()) {
      const category = categoryBuilder.object(excel.headers, row, validator);
      const productBase = productBuilder.object(excel.headers, row, validator);
      const pictures = picturesBuilder.list(excel.headers, row);
      const offer = offerBuilder.object(excel.headers, row);
      const media = mediaBuilder.list(excel.headers, row);
      const weight = weightBuilder.object(excel.headers, row);
      const wdDescription = wdDescriptionBuilder.object(excel.headers, row);
      const dimensions = dimensionsBuilder.object(excel.headers, row);
      
      const wd = Object.assign({}, wdDescription, {weight}, {dimensions});
      const product = Object.assign({}, productBase, category, {pictures}, {media}, {offer}, {wd})
      
      // console.log(category)
      // console.log(product)
    }
    
    console.log(validator.errors)
  });
});
