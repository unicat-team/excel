import { parse as parseXSLX } from 'node-xlsx';
import Excel from './excel/excel';
import Header from './excel/header';
import Cell from './excel/cell';

export default class StandardExcelBuilder {
  constructor(buffer) {
    this.data = parseXSLX(buffer)[0].data;
  }
  
  excel() {
    const headers = this.headers.map(this.headerBuilder);
    const cells = this.cells.map(this.cellBuilder);
      
    return new Excel(headers, cells);
  }
  
  headerBuilder(header) {
      const {name, index} = header;
      return new Header(name, index);
  }
  
  cellBuilder(cell) {
      const {rowIndex, columnIndex, value} = cell;
      return new Cell(rowIndex, columnIndex, value);
  }
  
  get headers() {
    return this.data[0]
      .map((name, index) => ({name, index}))
      .filter(this.isHeaderNotEmpty);
  }
  
  get cells() {
    return this.data
      .slice(1)
      .map((cells, rowIndex) => 
        ({cells: cells.map((value, columnIndex) => 
          ({rowIndex, columnIndex, value}))}))
      .filter(row => row.cells.every(this.isCellNotEmpty))
      .reduce((cells, row) => cells.concat(row.cells), [])
      .filter(this.isCellNotEmpty);
  }
  
  get isHeaderNotEmpty() {
    return header => header.name;
  }
  
  get isCellNotEmpty() {
    return cell => cell.value;
  }
  
}