import i18n from 'i18next';
import Backend from 'i18next-sync-fs-backend';
import { safeLoad } from 'js-yaml';
import { readFileSync } from 'fs';
i18n
  .init({resources: {
      en: {translation: safeLoad(readFileSync(`${__dirname}/locales/en.yml`))},
      ru: {translation: safeLoad(readFileSync(`${__dirname}/locales/ru.yml`))}
  }});
export default lang => i18n.getFixedT(lang).bind(i18n);