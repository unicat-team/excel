export default class BuildHandler {
  constructor(...validators) {
    this.validators = validators || [];
    this.errors = [];
  }
  
  apply(map, mapper, args) {
    const result = map.apply(mapper, args);
    const [headers, row] = args;
    const errors = this.validators.map(validator => validator.validate(result, mapper, row));
    this.errors.push.apply(this.errors, errors.filter(error => error.valid === false));
    
    return result;
  }
}