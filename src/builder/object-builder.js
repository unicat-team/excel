import Builder from './builder';
import ObjectMapper from '../map/object-mapper';

export default class ObjectBuilder extends Builder {
  
  add(name, map) {
    // return new ObjectBuilder(this.mappings.concat([new ObjectMapper(name, map)]));
    return super.add(new ObjectMapper(name, map)); 
  }
  
    object(headers, row, validator) {
      const result = super.map(headers, row, validator);
      return Object.assign.apply(null, [{}].concat(result));
    }
}