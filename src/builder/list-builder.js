import Builder from './builder';
import ListMapping from '../map/list-mapper';

export default class ListBuilder extends Builder {
    add(name, map) {
     return super.add(new ListMapping(name, map)); 
    }
    
    list(headers, row, validator) {
      const result = super.map(headers, row, validator);
      return Array.prototype.concat.apply([], result);
    }
}