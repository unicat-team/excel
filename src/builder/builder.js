import BuildHandler from './build-handler';

export default class Builder {
    constructor(mappings = []) {
        this.mappings = mappings;
    }
    
    add(mapper) {
        return new this.constructor(this.mappings.concat([mapper]));
    }
    
    map(headers, row, handler = new BuildHandler()) {
      return this.mappings.map(mapping => 
        new Proxy(mapping.map, handler)
          .call(mapping, headers, row)); 
    }
}



