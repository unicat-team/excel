import Validator from './validator';

export default class Required extends Validator {
    validate(value, mapping, row) {
        const {name} = mapping;
        
        if (this.name === name && value === null) {
            return {valid: false, message: mapping.name + ' must be not null in ' + row.index};
        }
        return {valid: true};
    }
}