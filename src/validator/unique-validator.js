import Validator from './validator';

export default class Unique extends Validator {
    constructor(name) {
        super(name);
        this.values = new Set();
    }
    validate(value, mapping, row) {
        const {name} = mapping;
        const normalized = JSON.stringify(value);
        if (name === this.name && this.values.has(normalized)) {
            return {valid: false, message: mapping.name + ' must be unique in ' + row.index};
        }
        this.values.add(normalized);
        return {valid: true};
    }
}