export default class Mapper {
    constructor(name, map) {
        this.name = name;
        this.mapFn = map;
    }
}