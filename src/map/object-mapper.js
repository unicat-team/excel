import Mapper from './mapper';

export default class ObjectMapper extends Mapper {
    map(headers, row) {
      const cell = row.find(cell => 
        headers.some(header => 
          header.name === this.name && cell.column === header.index));
          
      return cell ? this.mapFn(cell.value) : null;
    }
}

