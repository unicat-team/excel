import Mapper from './mapper';

export default class ListMapper extends Mapper {
    map(headers, row) {
      const cells = row.filter(cell => 
        headers.some(header => header.name === this.name && cell.column === header.index));
      
      return cells
        .map(cell => cell.value)
        .map(this.mapFn);
    }
}