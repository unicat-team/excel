export default class Header {
  constructor(name, index) {
    this.index = index;
    this.name = name;
  }
  
  get letter() {
    const letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return `${letters.charAt(Math.floor(this.index / 26 - 1))}${letters.charAt(this.index % 26)}`;
  }
}