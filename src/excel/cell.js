export default class Cell {
  constructor(row, column, value) {
    this.row = row;
    this.column = column;
    this.value = value;
  }
}