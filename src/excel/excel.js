export default class Excel {
  constructor(headers, cells) {
    this.headers = headers;
    this.cells = cells;
  }
  
  get rowsIndexes() {
    return this.cells
      .map(cell => cell.row)
      .filter((rowIndex, index, array) => array.indexOf(rowIndex) === index);
  }
  
  row(rowIndex) {
    const row = this.cells.filter(cell => cell.row === rowIndex);
    return Object.create(row, {index: {value: rowIndex}});
  }
  
  * rows() {
    yield *this.rowsIndexes.map(this.row.bind(this));
  } 
}